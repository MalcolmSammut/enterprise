namespace ItemStore.Migrations
{
    using FizzWare.NBuilder;
    using ItemStore.Models;
    using Microsoft.Ajax.Utilities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;



    internal sealed class Configuration : DbMigrationsConfiguration<ItemStore.Models.ApplicationDbContext>
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "ItemStore.Models.ApplicationDbContext";
        }
        protected override void Seed(ItemStore.Models.ApplicationDbContext context)
        {
            IList<ItemType> itemtypes = new List<ItemType>();

            var category = Builder<Category>.CreateListOfSize(5)
            .All()
                .With(c => c.name = "Category" + c.id.ToString())
            .Build();

            context.Categories.AddOrUpdate(c => c.id, category.ToArray());


            category.ForEach(c =>
            {
                itemtypes = Builder<ItemType>.CreateListOfSize(20)
                    .All()
                        .With(oi => oi.name = "ItemType" + c.id.ToString())
                        .With(oi => oi.imageURL = "https://picsum.photos/60")
                        .With(oi => oi.categoryId = c.id)
                    .Build();

                context.ItemTypes.AddOrUpdate(oi => oi.id, itemtypes.ToArray());
            });


            var priceGenerator = new RandomGenerator();
            db.ItemTypes.ForEach(i =>
            {
                var items = Builder<Item>.CreateListOfSize(10)
                    .All()
                        .With(it => it.price = priceGenerator.Next(1, 1000))
                        .With(it => it.quantity = priceGenerator.Next(1, 10))
                        .With(it => it.qualityId = 2)
                        .With(it => it.ownerId = "2cc27af5-2069-4133-ada5-4ade3f72945c")
                        .With(it => it.itemTypeId = i.id)
                        .With(it => it.date = DateTime.Now)
                     .Build();
                context.Items.AddOrUpdate(it => it.id, items.ToArray());
            });

            if (db.Qualities.Count() == 0)
            {
                context.Qualities.AddOrUpdate(x => x.id,
                new Quality() { name = "Excellent" },
                new Quality() { name = "Good" },
                new Quality() { name = "Poor" },
                new Quality() { name = "Bad" });
            }

        }
    }
}
