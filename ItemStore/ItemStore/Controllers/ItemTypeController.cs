﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dropbox.Api;
using Dropbox.Api.Files;
using ItemStore.Models;
using PagedList;

namespace ItemStore.Controllers
{
    [HandleError]
    public class ItemTypeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index(string search, int? i,int categoryId)
        {
            var category = db.Categories.Find(categoryId);
            if (category != null)
            {
                ViewBag.category = category;
                var list = search == null ? db.ItemTypes.Where(x => x.categoryId == categoryId) : db.ItemTypes.Where(x => x.name.StartsWith(search) && x.categoryId == categoryId);
                return View(list.ToList().ToPagedList(i ?? 1, 3));
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult Create(int id)
        {
            CategoryId = id;
            return View();
        }

        private static int CategoryId = -1;

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(HttpPostedFileBase file, [Bind(Include = "id,name,imageUrl,categoryId")] ItemType itemType)
        {
            string ApplicationName = "ItemStore";
            string accessToken = "cyl3tGx0a0AAAAAAAAAADHdErHVnqXHswLIFPd9o9FtpphheplFaYsAQrmMS1tqh";

            using (DropboxClient client = new DropboxClient(accessToken, new DropboxClientConfig(ApplicationName)))
            {
                string[] spitInputFileName = file.FileName.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
                string fileNameAndExtension = spitInputFileName[spitInputFileName.Length - 1];

                string[] fileNameAndExtensionSplit = fileNameAndExtension.Split('.');
                string originalFileName = fileNameAndExtensionSplit[0];
                string originalExtension = fileNameAndExtensionSplit[1];

                if (originalExtension == "jpeg" || originalExtension == "png" || originalExtension == "jpg" || originalExtension == "gif")
                {
                    string fileName = @"/Images/" + originalFileName + Guid.NewGuid().ToString().Replace("-", "") + "." + originalExtension;

                    var updated = client.Files.UploadAsync(
                        fileName,
                        mode: WriteMode.Overwrite.Overwrite.Instance,
                        body: file.InputStream).Result;

                    var result = client.Sharing.CreateSharedLinkWithSettingsAsync(fileName).Result.Url;

                    itemType.categoryId = CategoryId;
                    itemType.imageURL = result;

                    db.ItemTypes.Add(itemType);
                    db.SaveChanges();

                    return RedirectToAction("index", new { categoryId = CategoryId });
                }
                else
                {
                    ViewBag.Message = "File must be an image!";
                    return View();
                }

            }
        }


 
    }
}
