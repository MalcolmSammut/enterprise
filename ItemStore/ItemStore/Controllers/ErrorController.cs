﻿using System.Web.Mvc;

namespace ItemStore.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [Route("/Error/{ErrorCode}")]
        public ActionResult Error(int ErrorCode)
        {
            ViewBag.Error = ErrorCode;
            return View(ErrorCode);
        }

    }
}