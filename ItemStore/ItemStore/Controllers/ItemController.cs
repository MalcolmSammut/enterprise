﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ItemStore.Models;
using PagedList;
namespace ItemStore.Controllers
{
    [HandleError]
    public class ItemController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index(string search, int? i, int itemTypeId)
        {
            var itemtype = db.ItemTypes.Find(itemTypeId);
            if (itemtype != null)
            {
                ViewBag.itemtype = itemtype;
                ViewBag.Qualities = db.Qualities.ToList();
                ViewBag.owners = db.Users.ToList();
                var list = search == null ? db.Items.Where(x => x.itemTypeId == itemTypeId) : db.Items.Where(x => x.itemTypeId.ToString().StartsWith(search) && x.itemTypeId == itemTypeId);
                return View(list.ToList().ToPagedList(i ?? 1, 3));
            }
            else
            {
                return HttpNotFound();
            }

        }
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item items = db.Items.Find(id);
            ViewBag.Qualities = db.Qualities.ToList();
            if (items == null)
            {
                return HttpNotFound();
            }
            return View(items);
        }
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.Qualities = db.Qualities.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "id,quantity,price,qualityId,itemTypeId,ownerId,date")] Item items)
        {
            if (ModelState.IsValid)
            {
                var id = User.Identity.GetUserId();
                var userList = db.Items.Where(x => x.ownerId == id).ToList();
                foreach (var item in userList)
                {
                    if (item.price == items.price && item.qualityId == items.qualityId)
                    {
                        ViewBag.Error = "Price and Quality are the same change them";
                        ViewBag.Qualities = db.Qualities.ToList();
                        return View();
                    }
                }
                items.date = DateTime.Now;
                items.ownerId = id;
                db.Items.Add(items);
                db.SaveChanges();
                return RedirectToAction("Index", new { itemTypeId = items.itemTypeId });
            }

            return View(items);
        }
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item items = db.Items.Find(id);
            if (items == null)
            {
                return HttpNotFound();
            }
            ViewBag.Qualities = db.Qualities.ToList();
            return View(items);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "id,quantity,price,qualityId,itemTypeId,ownerId,date")] Item items)
        {
            if (ModelState.IsValid)
            {
                db.Entry(items).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { itemTypeId = items.itemTypeId });
            }
            return View(items);
        }


        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item items = db.Items.Find(id);
            if (items == null)
            {
                return HttpNotFound();
            }
            return View(items);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Item items = db.Items.Find(id);
            db.Items.Remove(items);
            db.SaveChanges();
            return RedirectToAction("Index",new { itemTypeId = items.itemTypeId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult Items(int? i)
        {
            ViewBag.itemtypes = db.ItemTypes;
            var items = db.Items.OrderByDescending(x => x.date).ToList();
            return View(items.ToPagedList(i ?? 1, 3));
        }

        public ItemType GetItemType(int id)
        {
            return db.ItemTypes.ToList().Find(x => x.id == id);
        }

        public ApplicationUser GetOwnerName(string id){
            return db.Users.ToList().Find(x => x.Id == id);
        }

        public String getQuality(int id)
        {
            return db.Qualities.ToList().Find(x => x.id == id).name;
        }

        public ActionResult UserItems(int? i, string userId)
        {
            ViewBag.itemtypes = db.ItemTypes;
            ViewBag.id = userId;
            var items = db.Items.Where(x=>x.ownerId==userId).OrderByDescending(x => x.date).ToList();
            return View(items.ToPagedList(i ?? 1, 3));
        }
    }
}
