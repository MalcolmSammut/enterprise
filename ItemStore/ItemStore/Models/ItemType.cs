﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ItemStore.Models
{
    public class ItemType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Required (ErrorMessage = "Name must be entered")]
        public string name { get; set; }

        [Required(ErrorMessage = "ImageURL must be entered")]
        public string imageURL { get; set; }

        public int categoryId { get; set; }

        public virtual ICollection<Item> items { get; set; }

    }
}