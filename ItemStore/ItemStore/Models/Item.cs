﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ItemStore.Models
{
    [Table("Items", Schema = "dbo")]
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Required]
        [Range(0, 10000)]
        public int quantity { get; set; }

        [Required]
        [Range(0, 10000000)]
        [Index(IsClustered = false, IsUnique = false)]
        public decimal price { get; set; }

        public DateTime date { get; set; }

        public int qualityId { get; set; }

        public int itemTypeId { get; set; }

        public string ownerId { get; set; }
    }
}